FROM python:3.6-slim

WORKDIR /app

ENV FLASK_APP=app.py
ENV POSTGRESQL_URL=postgresql://worker:worker@db/app

COPY ./requirements.txt .
RUN apt-get update && apt-get install -y \
    libpq-dev \
    build-essential \
    && rm -rf /var/lib/apt/lists/* \
    && pip install --no-cache-dir -r requirements.txt

EXPOSE 5000

COPY . .

ENTRYPOINT ["/bin/sh", "entrypoint.sh"]

